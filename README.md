**HOW TO USE**:

1. Copy the gitlab-ci.yml file into the root folder of your project.

2. Delete unwanted steps from the *include* and *stages* sections.

3. In your project's repository go to Settings > CI/CD > Variables and,  depending on which steps you included, set the following variables:

**Docker**:
    - DOCKERFILE_PATH
    - DOCKER_COMPOSE_FILE_PATH

**Aws/ECR**:
    - ECR_CI_REGISTRY_IMAGE
    - AWS_ACCESS_KEY_ID
    - AWS_SECRET_ACCESS_KEY

**Kubernetes**:
    - HELM_INGRESS_HOST
    - NAMESPACE

**Django**:
    - DJANGO_SECRET_KEY
    - DJANGO_ALLOWED_HOSTS
    - POSTGRES_HOST
    - POSTGRES_DB
    - POSTGRES_USER
    - POSTGRES_PASSWORD
